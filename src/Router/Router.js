import * as React from 'react';
import LoginPage from  '../Views/Login/LoginPage';
import RepoList from  '../Views/Repo/RepoList';
import SplashScreen from '../Views/SplashScreen/SplashScreen';
import Dashboard from '../Views/Dashboard/Dashboard';
import Dashboard2 from '../Views/Dashboard/Dashboard2';
import Dashboard3 from '../Views/Dashboard/Dashboard3';
import RepoDetailsComponent from  '../Views/Repo/RepoDetailsComponent';
import OverseasProgram from  '../Views/Dashboard/OverseasProgram';

import AxisLineChartScreen from "../Views/Charts/AxisLineChartScreen";
import MultipleChartScreen from "../Views/Charts/MultipleChartScreen";
import MovingWindowChartScreen from "../Views/Charts/MovingWindowChartScreen";
import BarChartScreen from "../Views/Charts/BarChartScreen";
import HorizontalBarChartScreen from "../Views/Charts/HorizontalBarChartScreen";
import BubbleChartScreen from "../Views/Charts/BubbleChartScreen";
import CandleStickChartScreen from "../Views/Charts/CandleStickChartScreen";
import CombinedChartScreen from "../Views/Charts/CombinedChartScreen";
import LineChartScreen from "../Views/Charts/LineChartScreen";
import LineChartGradientScreen from "../Views/Charts/LineChartGradientScreen";
import TimeSeriesLineChartScreen from "../Views/Charts/TimeSeriesLineChartScreen";
import PieChartScreen from "../Views/Charts/PieChartScreen";
import RadarChartScreen from "../Views/Charts/RadarChartScreen";
import ScatterChartScreen from "../Views/Charts/ScatterChartScreen";
import StackedBarChartScreen from "../Views/Charts/StackedBarChartScreen";
import ZeroLineChartScreen from "../Views/Charts/ZeroLineChartScreen";
import LiveUpdateChartScreen from "../Views/Charts/LiveUpdateChartScreen";
import GroupBarChartScreen from "../Views/Charts/GroupBarChartScreen";
import InfiniteScrollLineChartScreen from "../Views/Charts/InfiniteScrollLineChartScreen";
import LinkageChartScreen from "../Views/Charts/LinkageChartScreen";
import StockChartScreen from "../Views/Charts/StockChartScreen";
import ChartsListScreen from "../Views/Charts/ChartsListScreen";

var ExampleRoutes = {
  PieChartScreen: {
    name: 'PieChart',
    screen: PieChartScreen,
    description: 'Displays a PieChart',
  },
  BarChartScreen: {
    name: 'BarChart',
    screen: BarChartScreen,
    description: 'Displays a BarChart',
  },
  StackedBarChartScreen: {
    name: 'StackedBarChart',
    screen: StackedBarChartScreen,
    description: 'Displays a StackedBarChart',
  },
  LineChartScreen: {
    name: 'LineChart',
    screen: LineChartScreen,
    description: 'Displays a LineChart',
  },
  LineChartGradientScreen: {
    name: 'LineChartGradient',
    screen: LineChartGradientScreen,
    description: 'Displays a LineChart with Gradient',
  },
  RadarChartScreen: {
    name: 'RadarChart',
    screen: RadarChartScreen,
    description: 'Displays a RadarChart',
  },
  BubbleChartScreen: {
    name: 'BubbleChart',
    screen: BubbleChartScreen,
    description: 'Displays a BubbleChart',
  },
  ScatterChartScreen: {
    name: 'ScatterChart',
    screen: ScatterChartScreen,
    description: 'Displays a ScatterChart',
  },
  CandleStickChartScreen: {
    name: 'CandleStickChart',
    screen: CandleStickChartScreen,
    description: 'Displays a CandleStickChart',
  },
  StockChartScreen: {
    name: 'StockChartScreen',
    screen: StockChartScreen,
    description: 'Displays a StockChartScreen',
  },
  TimeSeriesLineChartScreen: {
    name: 'TimeSeriesLineChart',
    screen: TimeSeriesLineChartScreen,
    description: 'Displays a Time Series Line Chart with custom marker',
  },
  CombinedChartScreen: {
    name: 'CombinedChart',
    screen: CombinedChartScreen,
    description: 'Displays a CombinedChart with Bar and Line data.',
  },
  ZeroLineChartScreen: {
    name: 'ZeroLineChart',
    screen: ZeroLineChartScreen,
    description: 'Displays a zero-based BarChart.',
  },
  LiveUpdateChartScreen: {
    name: 'LiveUpdateChart',
    screen: LiveUpdateChartScreen,
    description: 'Live updating a line chart',
  },
  GroupBarChartScreen: {
    name: 'GroupBarChart',
    screen: GroupBarChartScreen,
    description: 'Displays a group BarChart',
  },
  HorizontalBarChartScreen: {
    name: 'HorizontalBarChart',
    screen: HorizontalBarChartScreen,
    description: 'Displays a HorizontalBarChart',
  },
  AxisLineChartScreen: {
    name: 'AxisLineChart',
    screen: AxisLineChartScreen,
    description: 'Displays a AxisLineChart',
  },
  MultipleChartScreen: {
    name: 'MultipleChartScreen',
    screen: MultipleChartScreen,
    description: 'Displays a correlated MultipleChartScreen',
  },
  MovingWindowChartScreen: {
    name: 'MovingWindowChartScreen',
    screen: MovingWindowChartScreen,
    description: 'Displays a MovingWindowChartScreen',
  },
  InfiniteScrollLineChartScreen: {
    name: 'InfiniteScrollLineChartScreen',
    screen: InfiniteScrollLineChartScreen,
    description: 'Displays a InfiniteScrollChartScreen',
  },
  LinkageChartScreen: {
    name: 'LinkageChartScreen',
    screen: LinkageChartScreen,
    description: 'Displays a LinkageChartScreen',
  },
};




import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { navigationRef } from './RootNavigation';

const Stack = createNativeStackNavigator();

function Router() {
  return (
    <NavigationContainer ref={navigationRef}>
      <Stack.Navigator screenOptions={{ headerTitleAlign: 'center', headerTintColor:'red' }} initialRouteName="SplashScreen">
        <Stack.Screen name="Login" component={LoginPage}  options={{ title: 'Login', headerShown:false }}/>
        <Stack.Screen name="RepoList" component={RepoList}  options={{ title: 'Repo List' }}/>
        <Stack.Screen name="SplashScreen" component={SplashScreen}  options={{ title: 'Splash List', headerShown:false ,
        headerBackVisible:false }}/>
            <Stack.Screen name="Dashboard" component={Dashboard}  options={{ title: 'Splash List', headerShown:false ,
        headerBackVisible:false }}/>
            <Stack.Screen name="Dashboard2" component={Dashboard2}  options={{ title: 'Splash List', headerShown:false , headerBackVisible:false }}/>
            <Stack.Screen name="Dashboard3" component={Dashboard3}  options={{ title: 'Splash List', headerShown:false , headerBackVisible:false }}/>
            <Stack.Screen name="RepoDetailsComponent" component={RepoDetailsComponent}  options={{ title: 'Repo Detail' }}/>
        <Stack.Screen name="ChartsList" component={ChartsListScreen} options={{headerShown:false,title: 'Repo Detail'}}  />
        <Stack.Screen name="OverseasProgram" component={OverseasProgram} options={{headerShown:false,title: 'Repo Detail'}}  />
        
        {Object.keys(ExampleRoutes).map((routeName) => (
        <Stack.Screen name={routeName} component={ExampleRoutes[routeName].screen}
                      key={routeName} />
      ))}
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default Router;