import React from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  processColor
} from 'react-native';

import {HorizontalBarChart} from 'react-native-charts-wrapper';

class HorizontalBarChartScreen extends React.Component {

  constructor() {
    super();

    this.state = {
      legend: {
        enabled: true,
        textSize: 14,
        form: 'LINE',
        formSize: 12,
        xEntrySpace: 5,
        yEntrySpace: 5,
        formToTextSpace: 5,
        wordWrapEnabled: true,
        maxSizePercent: 0.5,
      },
      data: {
        dataSets: [{
          barWidth:9,
          values: [
            {y: 100}, {y: 105}, {y: 102}, {y: 110}, {y: 114}, {y: 109}, {y: 105}, {y: 99},
             {y: 95}, {y: 109}, {y: 105}, {y: 99}, {y: 95},{y: 105}, {y: 99}, {y: 95}
            ],
          label: 'Bar dataSet',
          config: {
            barWidth:8,
            ///color: processColor('red'),
            colors:[processColor('red'),processColor('green'),processColor('blue')],
            barShadowColor: processColor('lightgrey'),
            highlightAlpha: 100,
            highlightColor: processColor('red'),
          }
        }], config: {
          barWidth:0.6,
        }
      },
      xAxis: {
        valueFormatter: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep','Jul', 'Aug', 'Sep','oho','Aug', 'Sep','oho'],
        position: 'BOTTOM',
        granularityEnabled: true,
        granularity: 1,
        labelCount: 16,
      },
      yAxis: {left:{axisMinimum: 0}}
    };
  }

  handleSelect(event) {
    let entry = event.nativeEvent
    if (entry == null) {
      this.setState({...this.state, selectedEntry: null})
    } else {
      this.setState({...this.state, selectedEntry: JSON.stringify(entry)})
    }

    console.log(event.nativeEvent)
  }


  render() {
    return (
      <View style={{flex: 1}}>

        <View style={{height:80}}>
          <Text> selected entry</Text>
          <Text> {this.state.selectedEntry}</Text>
        </View>


        <View style={styles.container}>
          <HorizontalBarChart
            style={styles.chart}
            data={this.state.data}
            xAxis={this.state.xAxis}
            yAxis={this.state.yAxis}
            animation={{durationX: 2000}}
            legend={this.state.legend}
            gridBackgroundColor={processColor('#ffffff')}
            drawBarShadow={false}
            drawValueAboveBar={true}
            drawHighlightArrow={true}
            onSelect={this.handleSelect.bind(this)}
            onChange={(event) => console.log(event.nativeEvent)}
          barWidth={9}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F5FCFF'
  },
  chart: {
    flex: 1
  }
});

export default HorizontalBarChartScreen;
