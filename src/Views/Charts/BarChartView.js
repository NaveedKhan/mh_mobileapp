import React from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,ScrollView,Dimensions,
  processColor,
} from 'react-native';
import { BarChart} from "react-native-gifted-charts";

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

function BarChartView(dataSource,chartLabelName,horizental){
   
    const  labelTextStyle=
    {
      transform: [{ rotate: '-30deg'}],
      fontSize:10
    };
    const  labelTextStyle2=
    {
     // transform: [{ rotate: '-30deg'}],
      fontSize:10
    };
    
    const barData3 = [
        {value: 12, label: 'Masa', labelTextStyle:labelTextStyle2},
        {value: 40, label: 'Tasdsa', frontColor: '#177AD5', labelTextStyle:labelTextStyle2},
        {value: 30, label: 'Wsdadsda', frontColor: '#177AD5', labelTextStyle:labelTextStyle2},
        {value: 90, label: 'T', labelTextStyle:labelTextStyle2},
        {value: 11, label: 'F', frontColor: '#177AD5', labelTextStyle:labelTextStyle2},
        {value: 66, label: 'S', labelTextStyle:labelTextStyle2},
        {value: 12, label: 'S', labelTextStyle:labelTextStyle2},
        {value: 12, label: 'M',labelTextStyle:labelTextStyle2},
        {value: 40, label: 'T', frontColor: '#177AD5',labelTextStyle:labelTextStyle2},
        {value: 30, label: 'W', frontColor: '#177AD5',labelTextStyle:labelTextStyle2},
        {value: 90, label: 'T',labelTextStyle:labelTextStyle2},
        {value: 11, label: 'F', frontColor: '#177AD5',labelTextStyle:labelTextStyle2},
        {value: 66, label: 'S',labelTextStyle:labelTextStyle2},
        {value: 12, label: 'S',labelTextStyle:labelTextStyle2},
        {value: 66, label: 'S',labelTextStyle:labelTextStyle2},
        {value: 12, label: 'Suuu',labelTextStyle:labelTextStyle2},
    ]
    const ProgreeByResult = [
        {
          value: 230,
          label: 'Jan',
          frontColor: '#4ABFF4',
          sideColor: '#23A7F3',
          topColor: '#92e6f6',
          labelTextStyle:labelTextStyle,
          onPress:called
        },
        {
          value: 180,
          label: 'Feb',
          frontColor: '#79C3DB',
          sideColor: '#68BCD7',
          topColor: '#9FD4E5',
          labelTextStyle:labelTextStyle,
          onPress:called
        },
        {
          value: 195,
          label: 'Mar',
          frontColor: '#28B2B3',
          sideColor: '#0FAAAB',
          topColor: '#66C9C9',
          labelTextStyle:labelTextStyle,
          onPress:called
        },
        {
          value: 250,
          label: 'Apr',
          frontColor: '#4ADDBA',
          sideColor: '#36D9B2',
          topColor: '#7DE7CE',
          labelTextStyle:labelTextStyle
        },
        {
          value: 320,
          label: 'May',
          frontColor: '#91E3E3',
          sideColor: '#85E0E0',
          topColor: '#B0EAEB',
          labelTextStyle:labelTextStyle
        },
        {
          value: 230,
          label: 'Jan',
          frontColor: '#4ABFF4',
          sideColor: '#23A7F3',
          topColor: '#92e6f6',
          labelTextStyle:labelTextStyle
        },
        {
          value: 180,
          label: 'Feb',
          frontColor: '#79C3DB',
          sideColor: '#68BCD7',
          topColor: '#9FD4E5',
          labelTextStyle:labelTextStyle
        },
        {
          value: 195,
          label: 'Mar',
          frontColor: '#28B2B3',
          sideColor: '#0FAAAB',
          topColor: '#66C9C9',
          labelTextStyle:labelTextStyle
        },
        {
          value: 250,
          label: 'Apr',
          frontColor: '#4ADDBA',
          sideColor: '#36D9B2',
          topColor: '#7DE7CE',
          labelTextStyle:labelTextStyle
        },
        {
          value: 320,
          label: 'May',
          frontColor: '#91E3E3',
          sideColor: '#85E0E0',
          topColor: '#B0EAEB',
          labelTextStyle:labelTextStyle
        },
        {
          value: 180,
          label: 'Feb',
          frontColor: '#79C3DB',
          sideColor: '#68BCD7',
          topColor: '#9FD4E5',
          labelTextStyle:labelTextStyle
        },
        {
          value: 195,
          label: 'Mar',
          frontColor: '#28B2B3',
          sideColor: '#0FAAAB',
          topColor: '#66C9C9',
          labelTextStyle:labelTextStyle
        },
        {
          value: 250,
          label: 'Apr',
          frontColor: '#4ADDBA',
          sideColor: '#36D9B2',
          topColor: '#7DE7CE',
          labelTextStyle:labelTextStyle
        },
        {
          value: 320,
          label: 'May',
          frontColor: '#91E3E3',
          sideColor: '#85E0E0',
          topColor: '#B0EAEB',
          labelTextStyle:labelTextStyle
        },
        {
          value: 195,
          label: 'Mar',
          frontColor: '#28B2B3',
          sideColor: '#0FAAAB',
          topColor: '#66C9C9',
          labelTextStyle:labelTextStyle
        },
        {
          value: 250,
          label: 'Apr',
          frontColor: '#4ADDBA',
          sideColor: '#36D9B2',
          topColor: '#7DE7CE',
          labelTextStyle:labelTextStyle
        },
        {
          value: 320,
          label: 'May',
          frontColor: '#91E3E3',
          sideColor: '#85E0E0',
          topColor: '#B0EAEB',
          labelTextStyle:labelTextStyle
        },  {
          value: 195,
          label: 'Mar',
          frontColor: '#28B2B3',
          sideColor: '#0FAAAB',
          topColor: '#66C9C9',
          labelTextStyle:labelTextStyle
        },
        {
          value: 250,
          label: 'Apr',
          frontColor: '#4ADDBA',
          sideColor: '#36D9B2',
          topColor: '#7DE7CE',
          labelTextStyle:labelTextStyle
        },
        {
          value: 320,
          label: 'May',
          frontColor: '#91E3E3',
          sideColor: '#85E0E0',
          topColor: '#B0EAEB',
          labelTextStyle:labelTextStyle
        },  {
          value: 195,
          label: 'Mar',
          frontColor: '#28B2B3',
          sideColor: '#0FAAAB',
          topColor: '#66C9C9',
          labelTextStyle:labelTextStyle
        }
        
      ];

      const called=(el)=>{
        debugger;
        console.log('clicked',el);
      }

const HorizentalBar=()=>{
  return <View style={{flex:1,backgroundColor:'green',
  paddingLeft:20,
  alignItems:'center',margin:10,paddingTop:10,paddingBottom:10,height:690,padding:5}}> 
   <BarChart
            
        horizontal
        barWidth={15}
      // noOfSections={10}
       maxValue={100}
        spacing={5}
        width={400}
        height={width-90}
        barBorderRadius={4}
        frontColor="lightgray"
        data={barData3}
        yAxisThickness={1}
        xAxisThickness={1}
       // xAxisIndicesWidth={60}
        //labelWidth={30}
        labelTextStyle={{marginRight:10}}
        side="left"
        cappedBars
     //  isThreeD
       //isAnimated
       //xAxisIndicesHeight={700}
       //yAxisIndicesWidth={600}
       yAxisTextStyle={{fontSize:9}}
    />
    </View>
}

const VerticalBar=()=>{
    return    <ScrollView horizontal={horizental} style={{flex:1,height:300}}>
    <BarChart
    showYAxisIndices
    barBorderRadius={4}
    noOfSections={8}
    maxValue={400}
    showScrollIndicator
    width={800}
    data={ProgreeByResult}
    barWidth={15}
    disableScroll={false}
    sideWidth={15}
    horizontal={horizental}
    isThreeD 
    spacing={15}
    side="right"
    yAxisTextStyle={{fontSize:9}}
    />
</ScrollView>
}

    return (<View style={{flex:1,backgroundColor:'green'}}> 
 {horizental? HorizentalBar():VerticalBar()}
   </View>
   )
}

export default BarChartView;