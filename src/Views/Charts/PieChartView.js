import React from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  processColor,
} from 'react-native';
import {PieChart} from 'react-native-charts-wrapper';




function PieChartView(dataSource,chartLabelName){
debugger;

    const chartData = {
        legend: {
          enabled: true,
          textSize: 12,
          form: 'CIRCLE',
          horizontalAlignment: "CENTER",
          verticalAlignment: "BOTTOM",
          orientation: "HORIZONTAL",
          wordWrapEnabled: true
        },
        data: {
          dataSets: [{
            values:dataSource.values,
            label: '',
            config: {
              colors: dataSource.colors,
              valueTextSize: 11,
              valueTextColor: processColor('white'),
              sliceSpace: 2,
              selectionShift: 13,
              // xValuePosition: "OUTSIDE_SLICE",
              // yValuePosition: "OUTSIDE_SLICE",
              valueFormatter: "#.#'%'",
              valueLineColor: processColor('black'),
              valueLinePart1Length: 0.5
            }
          }],
        },
        highlights: [],
        description: {
          text: 'This is Pie chart description',
          textSize: 15,
          textColor: processColor('darkgray'),
  
        }
      };

     const handleSelect=(event)=> {
        let entry = event.nativeEvent
        if (entry == null) {
          console.log('no value')
        } else {
            console.log(' value',JSON.stringify(entry))
        }
        console.log(event.nativeEvent)
      }

      return(
        <View style={styles.container}>
                <PieChart
                    style={styles.chart}
                    logEnabled={true}
                    chartBackgroundColor={processColor('white')}
                    chartDescription={chartData.description}
                    data={chartData.data}
                    legend={chartData.legend}
                    highlights={chartData.highlights}

                    extraOffsets={{left: 5, top: 5, right: 5, bottom: 5}}

                    entryLabelColor={processColor('white')}
                    entryLabelTextSize={11}
                    entryLabelFontFamily={'HelveticaNeue-Medium'}
                    drawEntryLabels={true}
                    rotationEnabled={true}
                    rotationAngle={45}
                    usePercentValues={true}
                    styledCenterText={{text:chartLabelName, color: processColor('black'), fontFamily: 'HelveticaNeue-Medium', size: 13}}
                    centerTextRadiusPercent={100}
                    holeRadius={40}
                    holeColor={processColor('#f0f0f0')}
                    transparentCircleRadius={45}
                    transparentCircleColor={processColor('#f0f0f088')}
                    maxAngle={360}
                    onSelect={handleSelect}
                    onChange={(event) => console.log(event.nativeEvent)}
                />
      </View>
      );


}

export default PieChartView;


const styles = {
    container: {
      flex: 1,
      padding:3,
      borderRadius:5,
      borderWidth:1,
      borderColor:'gray',
      overflow:'hidden'
      //backgroundColor:'green'
    },
    chart: {
      flex: 1,
      
    }
  };