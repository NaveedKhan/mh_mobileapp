import React, { useState } from "react";
import { View, StyleSheet,Dimensions,Text ,Image} from "react-native";
import {
    FormControl,
    Select,
    Container,
    CheckIcon,
    WarningOutlineIcon,
    Center,
    NativeBaseProvider,
    ScrollView,
  } from "native-base"
import { CustomHeaderWithText } from "../Common/CustomHeaderWithText";


import PieChartView from "../Charts/PieChartView";

import {ProgramWiseBeneficieries} from  '../../Constants/JsonData/ProgramWiseBeneficieries';
import {OverAllBenficieries} from  '../../Constants/JsonData/OverAllBenficieries';
import {ProgramWiseHeaders} from  '../../Constants/JsonData/ProgramWiseHeaders';
import BarChartView from "../Charts/BarChartView";

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;





function OverseasProgram ()  {
 
 const [selectedValue, setSelectedValue] = useState("java");
 let [service, setService] = React.useState("");
const FilterArr = ProgramWiseHeaders.headersdata;

const renderFilterCardColumn = (obj) => {
    return   <View style={{flex:.29,alignItems:'center',backgroundColor:'white',
    justifyContent:'center',borderWidth:0.7,borderRadius:6,borderColor:'#b5afaf',paddingBottom:3,elevation:3}}>
    <View style={{ height:height*0.08,alignItems:'center',backgroundColor:'transparent',alignSelf:'center'}}>
        <Image
        source={{uri:obj.ImageUrl}}
        style={{
        resizeMode: 'contain',
        width: width* 0.25,
       height:height*0.09
        }}
         />
         
    </View>
    <View style={{alignItems:'center',paddingTop:10}}>
        <Text >{obj.Name}</Text>
    </View>
</View>
}

const  labelTextStyle=
{
  transform: [{ rotate: '-30deg'}],
  fontSize:10,textAlign:'center'
};


const ProgreeByResult = [
    {
      value: 230,
      label: 'Jan',
      frontColor: '#4ABFF4',
      sideColor: '#23A7F3',
      topColor: '#92e6f6',
      labelTextStyle:labelTextStyle,
      onPress:called
    },
    {
      value: 180,
      label: 'Feb',
      frontColor: '#79C3DB',
      sideColor: '#68BCD7',
      topColor: '#9FD4E5',
      labelTextStyle:labelTextStyle,
      onPress:called
    },
    {
      value: 195,
      label: 'Mar',
      frontColor: '#28B2B3',
      sideColor: '#0FAAAB',
      topColor: '#66C9C9',
      labelTextStyle:labelTextStyle,
      onPress:called
    },
    {
      value: 250,
      label: 'Apr',
      frontColor: '#4ADDBA',
      sideColor: '#36D9B2',
      topColor: '#7DE7CE',
      labelTextStyle:labelTextStyle
    },
    {
      value: 320,
      label: 'May',
      frontColor: '#91E3E3',
      sideColor: '#85E0E0',
      topColor: '#B0EAEB',
      labelTextStyle:labelTextStyle
    },
    {
      value: 230,
      label: 'Jan',
      frontColor: '#4ABFF4',
      sideColor: '#23A7F3',
      topColor: '#92e6f6',
      labelTextStyle:labelTextStyle
    },
    {
      value: 180,
      label: 'Feb',
      frontColor: '#79C3DB',
      sideColor: '#68BCD7',
      topColor: '#9FD4E5',
      labelTextStyle:labelTextStyle
    },
    {
      value: 195,
      label: 'Mar',
      frontColor: '#28B2B3',
      sideColor: '#0FAAAB',
      topColor: '#66C9C9',
      labelTextStyle:labelTextStyle
    },
    {
      value: 250,
      label: 'Apr',
      frontColor: '#4ADDBA',
      sideColor: '#36D9B2',
      topColor: '#7DE7CE',
      labelTextStyle:labelTextStyle
    },
    {
      value: 320,
      label: 'May',
      frontColor: '#91E3E3',
      sideColor: '#85E0E0',
      topColor: '#B0EAEB',
      labelTextStyle:labelTextStyle
    },
    {
      value: 180,
      label: 'Feb',
      frontColor: '#79C3DB',
      sideColor: '#68BCD7',
      topColor: '#9FD4E5',
      labelTextStyle:labelTextStyle
    },
    {
      value: 195,
      label: 'Mar',
      frontColor: '#28B2B3',
      sideColor: '#0FAAAB',
      topColor: '#66C9C9',
      labelTextStyle:labelTextStyle
    },
    {
      value: 250,
      label: 'Apr',
      frontColor: '#4ADDBA',
      sideColor: '#36D9B2',
      topColor: '#7DE7CE',
      labelTextStyle:labelTextStyle
    },
    {
      value: 320,
      label: 'May',
      frontColor: '#91E3E3',
      sideColor: '#85E0E0',
      topColor: '#B0EAEB',
      labelTextStyle:labelTextStyle
    },
    {
      value: 195,
      label: 'Mar',
      frontColor: '#28B2B3',
      sideColor: '#0FAAAB',
      topColor: '#66C9C9',
      labelTextStyle:labelTextStyle
    },
    {
      value: 250,
      label: 'Apr',
      frontColor: '#4ADDBA',
      sideColor: '#36D9B2',
      topColor: '#7DE7CE',
      labelTextStyle:labelTextStyle
    },
    {
      value: 320,
      label: 'May',
      frontColor: '#91E3E3',
      sideColor: '#85E0E0',
      topColor: '#B0EAEB',
      labelTextStyle:labelTextStyle
    },  {
      value: 195,
      label: 'Mar',
      frontColor: '#28B2B3',
      sideColor: '#0FAAAB',
      topColor: '#66C9C9',
      labelTextStyle:labelTextStyle
    },
    {
      value: 250,
      label: 'Apr',
      frontColor: '#4ADDBA',
      sideColor: '#36D9B2',
      topColor: '#7DE7CE',
      labelTextStyle:labelTextStyle
    },
    {
      value: 320,
      label: 'May',
      frontColor: '#91E3E3',
      sideColor: '#85E0E0',
      topColor: '#B0EAEB',
      labelTextStyle:labelTextStyle
    },  {
      value: 195,
      label: 'Mar',
      frontColor: '#28B2B3',
      sideColor: '#0FAAAB',
      topColor: '#66C9C9',
      labelTextStyle:labelTextStyle
    }
    
  ];

  
  const called=(el)=>{
    debugger;
    console.log('clicked',el);
  }


  return (
    <View style={{flex:1,backgroundColor:'lighgray'}}>
    <NativeBaseProvider>
        <CustomHeaderWithText leftIcon={true} showMenu text ="Overseas Program"  />
        <ScrollView scrollEnabled showsHorizontalScrollIndicator={false} showsVerticalScrollIndicator={false}>
            
            <View style={[styles.container,{backgroundColor:'transparent'}]}>
              <View style={{justifyContent:'center',alignItems:'stretch',elevation:1}}>
                  <FormControl isRequired style={{borderColor:'gray',borderWidth:0,alignItems:'center',justifyContent:'center',marginLeft:0,margin:3}}>
                        {/* <FormControl.Label>Choose service</FormControl.Label> */}
                        <Select
                        //minWidth="300"
                        fontSize={15}
                        width={width-10}
                        selectedValue={service}
                        accessibilityLabel="Choose Country"
                        placeholder="Choose Country"
                        _selectedItem={{
                            bg: "teal.600",
                            endIcon: <CheckIcon size={5} />,
                        }}
                        mt="1"
                        onValueChange={(itemValue) => setService(itemValue)}
                        >
                        <Select.Item label="UX Research" value="ux" />
                        <Select.Item label="Web Development" value="web" />
                        <Select.Item label="Cross Platform Development" value="cross" />
                        <Select.Item label="UI Designing" value="ui" />
                        <Select.Item label="Backend Development" value="backend" />
                        </Select>
                        <FormControl.ErrorMessage leftIcon={<WarningOutlineIcon size="xs" />}>
                        Please make a selection!
                        </FormControl.ErrorMessage>
                    </FormControl>
              </View>
          
                <View style={{flexDirection:'row',backgroundColor:'transparent',alignItems:'stretch',padding:1, marginTop:10,justifyContent:'space-evenly'}}>
                    {renderFilterCardColumn(FilterArr[0])}
                    {renderFilterCardColumn(FilterArr[1])}
                    {renderFilterCardColumn(FilterArr[2])}
                </View>

                <View style={{flexDirection:'row',backgroundColor:'transparent',alignItems:'stretch',padding:1, marginTop:10,justifyContent:'space-evenly'}}>
                    {renderFilterCardColumn(FilterArr[3])}
                    {renderFilterCardColumn(FilterArr[4])}
                    {renderFilterCardColumn(FilterArr[5])}
                </View>

                <View style={{justifyContent:'center',
                              alignItems:'stretch',height:height*0.5,
                              padding:5,margin:5,
                              borderColor:'lightgray',
                              borderRadius:6, overflow:'hidden'}}>
                   {PieChartView(ProgramWiseBeneficieries,"Programme Wise Beneficiaries of 2021")}
                </View>
               <View style={{justifyContent:'center',
                              alignItems:'stretch',height:height*0.5,
                              padding:5,margin:5,
                              borderColor:'lightgray',
                              borderRadius:6, overflow:'hidden'}}>
                    {PieChartView(OverAllBenficieries,"Overall Beneficiaries of 2021")}
                </View>
      
                 <View style={{justifyContent:'center',
                              alignItems:'stretch',
                              padding:5,margin:5,backgroundColor:'white',borderWidth:1,
                              borderColor:'lightgray',height:270,
                              borderRadius:6, overflow:'hidden'}}>
                    {BarChartView(OverAllBenficieries,"ssa")}
                </View>

                     <View style={{backgroundColor:'blue',  
                     borderRadius:6, overflow:'hidden',margin:5,paddingTop:20,justifyContent:'flex-start'}}>
                    {BarChartView(OverAllBenficieries,"ssa",true)}
                </View>
          
          
            </View>
        </ScrollView>
    </NativeBaseProvider>

    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    //paddingTop: 40,
    //alignItems: "center",
   // justifyContent:'center'
  }
});

export default OverseasProgram;