import React, { useState, useEffect } from 'react';
import { View, Text, Image, Dimensions, TouchableHighlight,TextInput, StatusBar, ScrollView,PermissionsAndroid,Platform } from 'react-native';
import { useSelector, useDispatch } from 'react-redux'
import { useNavigation } from '@react-navigation/native'
import { BarChart, LineChart, PieChart } from "react-native-gifted-charts";

import { repoActions } from '../../actions';
import { ProgreeByResult } from '../../Constants/JsonData/ProgreeByResult';



function Dashboard(){
    const data=[ {value:50}, {value:80}, {value:90}, {value:70} ]

    const aas=()=>{
     debugger;
    }
    const barData = [
        {
          value: 40,
          label: 'Jan',
          spacing: 2,
          labelWidth: 30,
          labelTextStyle: {color: 'gray'},
          frontColor: '#177AD5',
          dataPointText:'40'
          
        },
        {value: 20, frontColor: '#ED6665'},
        {
          value: 50,
          label: 'Feb',
          spacing: 2,
          labelWidth: 30,
          labelTextStyle: {color: 'gray'},
          frontColor: '#177AD5',
          dataPointText:'40'
        },
        {value: 40,label: 'Feb', frontColor: '#ED6665'},
        {
          value: 75,
          label: 'Mar',
          spacing: 2,
          labelWidth: 30,
          labelTextStyle: {color: 'gray'},
          frontColor: '#177AD5',
          dataPointText:'40'
        },
        {value: 25,label: 'Feb', frontColor: '#ED6665'},
        {
          value: 30,
          label: 'Apr',
          spacing: 2,
          labelWidth: 30,
          labelTextStyle: {color: 'gray'},
          frontColor: '#177AD5',
          dataPointText:'40'
        },
        {value: 20, label: 'Feb',frontColor: '#ED6665'},
        {
          value: 60,
          label: 'May',
          spacing: 2,
          labelWidth: 30,
          labelTextStyle: {color: 'gray'},
          frontColor: '#177AD5',
        },
        {value: 40, label: 'Feb',frontColor: '#ED6665'},
        {
          value: 65,
          label: 'Jun',
          spacing: 2,
          labelWidth: 30,
          labelTextStyle: {color: 'gray'},
          frontColor: '#177AD5',
        },
        {value: 30, label: 'Feb',frontColor: '#ED6665'},
      ];
      const barData2 = [
        {
          value: 230,
          label: 'Jan',
          frontColor: '#4ABFF4',
          sideColor: '#23A7F3',
          topColor: '#92e6f6',
          labelTextStyle:
                      {
                        transform: [{ rotate: '-30deg'}],
                        fontSize:10,textAlign:'center'
                      }
        },
        {
          value: 180,
          label: 'Feb',
          frontColor: '#79C3DB',
          sideColor: '#68BCD7',
          topColor: '#9FD4E5',
          labelTextStyle:
                      {
                        transform: [{ rotate: '-30deg'}],
                        fontSize:10,textAlign:'center'
                      }
        },
        {
          value: 195,
          label: 'Mar',
          frontColor: '#28B2B3',
          sideColor: '#0FAAAB',
          topColor: '#66C9C9',
          labelTextStyle:
                      {
                        transform: [{ rotate: '-30deg'}],
                        fontSize:10,textAlign:'center'
                      }
        },
        {
          value: 250,
          label: 'Apr',
          frontColor: '#4ADDBA',
          sideColor: '#36D9B2',
          topColor: '#7DE7CE',
        },
        {
          value: 320,
          label: 'May',
          frontColor: '#91E3E3',
          sideColor: '#85E0E0',
          topColor: '#B0EAEB',
        },
        {
          value: 230,
          label: 'Jan',
          frontColor: '#4ABFF4',
          sideColor: '#23A7F3',
          topColor: '#92e6f6',
        },
        {
          value: 180,
          label: 'Feb',
          frontColor: '#79C3DB',
          sideColor: '#68BCD7',
          topColor: '#9FD4E5',
        },
        {
          value: 195,
          label: 'Mar',
          frontColor: '#28B2B3',
          sideColor: '#0FAAAB',
          topColor: '#66C9C9',
        },
        {
          value: 250,
          label: 'Apr',
          frontColor: '#4ADDBA',
          sideColor: '#36D9B2',
          topColor: '#7DE7CE',
        },
        {
          value: 320,
          label: 'May',
          frontColor: '#91E3E3',
          sideColor: '#85E0E0',
          topColor: '#B0EAEB',
        },
        {
          value: 180,
          label: 'Feb',
          frontColor: '#79C3DB',
          sideColor: '#68BCD7',
          topColor: '#9FD4E5',
        },
        {
          value: 195,
          label: 'Mar',
          frontColor: '#28B2B3',
          sideColor: '#0FAAAB',
          topColor: '#66C9C9',
        },
        {
          value: 250,
          label: 'Apr',
          frontColor: '#4ADDBA',
          sideColor: '#36D9B2',
          topColor: '#7DE7CE',
        },
        {
          value: 320,
          label: 'May',
          frontColor: '#91E3E3',
          sideColor: '#85E0E0',
          topColor: '#B0EAEB',
        },
        {
          value: 195,
          label: 'Mar',
          frontColor: '#28B2B3',
          sideColor: '#0FAAAB',
          topColor: '#66C9C9',
        },
        {
          value: 250,
          label: 'Apr',
          frontColor: '#4ADDBA',
          sideColor: '#36D9B2',
          topColor: '#7DE7CE',
        },
        {
          value: 320,
          label: 'May',
          frontColor: '#91E3E3',
          sideColor: '#85E0E0',
          topColor: '#B0EAEB',
        },  {
          value: 195,
          label: 'Mar',
          frontColor: '#28B2B3',
          sideColor: '#0FAAAB',
          topColor: '#66C9C9',
        },
        {
          value: 250,
          label: 'Apr',
          frontColor: '#4ADDBA',
          sideColor: '#36D9B2',
          topColor: '#7DE7CE',
        },
        {
          value: 320,
          label: 'May',
          frontColor: '#91E3E3',
          sideColor: '#85E0E0',
          topColor: '#B0EAEB',
        },  {
          value: 195,
          label: 'Mar',
          frontColor: '#28B2B3',
          sideColor: '#0FAAAB',
          topColor: '#66C9C9',
        }
        
      ];
      const renderTitle = () => {
        return(
          <View style={{marginBottom:10}}>
          <Text
            style={{
              color: 'white',
              fontSize: 20,
              fontWeight: 'bold',
              textAlign: 'center',
            }}>
            Chart title goes here
          </Text>
          <View
            style={{
              flex: 1,
              flexDirection: 'row',
              justifyContent: 'space-evenly',
              marginTop: 24,
             // backgroundColor: 'yellow',
            }}>
            <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <View
                style={{
                  height: 12,
                  width: 12,
                  borderRadius: 6,
                  backgroundColor: '#177AD5',
                  marginRight: 8,
                }}
              />
              <Text
                style={{
                  width: 60,
                  height: 16,
                  color: 'lightgray',
                }}>
                Point 01
              </Text>
            </View>
            <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <View
                style={{
                  height: 12,
                  width: 12,
                  borderRadius: 6,
                  backgroundColor: '#ED6665',
                  marginRight: 8,
                }}
              />
              <Text
                style={{
                  width: 60,
                  height: 16,
                  color: 'lightgray',
                }}>
                Point 02
              </Text>
            </View>
            <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <View
                style={{
                  height: 12,
                  width: 12,
                  borderRadius: 6,
                  backgroundColor: '#ED6665',
                  marginRight: 8,
                }}
              />
              <Text
                style={{
                  width: 60,
                  height: 16,
                  color: 'lightgray',
                }}>
                Point 02
              </Text>
            </View>
            <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <View
                style={{
                  height: 12,
                  width: 12,
                  borderRadius: 6,
                  backgroundColor: '#ED6665',
                  marginRight: 8,
                }}
              />
              <Text
                style={{
                  width: 60,
                  height: 16,
                  color: 'lightgray',
                }}>
                Point 02
              </Text>
            </View>
            <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <View
                style={{
                  height: 12,
                  width: 12,
                  borderRadius: 6,
                  backgroundColor: '#ED6665',
                  marginRight: 8,
                }}
              />
              <Text
                style={{
                  width: 60,
                  height: 16,
                  color: 'lightgray',
                }}>
                Point 02
              </Text>
            </View>
          </View>
        </View>
        )
    }
    function called  (el){
      debugger;
      console.log('clicked',el);
    }

    return (
        <View style={{flex:1,backgroundColor:'#e8e8e8'}}>
            <ScrollView scrollEnabled contentContainerStyle={{justifyContent:'space-between',alignItems:'center'}} style={{flex:1}}>
            <View style={{flex:1,backgroundColor:'#e8e8e8', justifyContent:'space-between',alignItems:'stretch'}}>
              {/* <BarChart data = {data}  /> */}
              <View
                  style={{
                    backgroundColor: '#333340',
                    paddingBottom: 40,
                    borderRadius: 10,
                    height:400
                  }}>
                      {renderTitle()}
                        <BarChart
                    data={barData}
                    barWidth={8}
                    spacing={24}
                    roundedTop
                    roundedBottom

                    hideRules
                    xAxisThickness={0}
                    yAxisThickness={0}
                    yAxisTextStyle={{color: 'gray'}}
                    noOfSections={3}
                    maxValue={75}
                    isAnimated
                  />
            </View>

            <View style={{backgroundColor:'white',borderColor:'lightgray',borderWidth:1,
             padding:5, paddingTop:10, height:270,margin:5,borderRadius:6,marginTop:10}}>
              <ScrollView horizontal style={{flex:1,height:300}}>
              <BarChart
                  showFractionalValue
                  showYAxisIndices
                  barBorderRadius={4}
                  noOfSections={8}
                  maxValue={400}
                  width={800}
                  data={ProgreeByResult}
                  barWidth={15}
                  disableScroll={false}
                  sideWidth={15}
                  isThreeD 
                  spacing={15}
                  side="right"
                  yAxisTextStyle={{fontSize:9}}
                  />
             </ScrollView>
        </View>

             <View style={{backgroundColor: '#1A3461',marginTop:50,paddingTop:40,      height:400}}>
             <LineChart
                initialSpacing={0}
                data={[
                  {value: 0, dataPointText: '0'},
                  {value: 20, dataPointText: '20'},
                  {value: 18, dataPointText: '18'},
                  {value: 40, dataPointText: '40'},
                  {value: 36, dataPointText: '36'},
                  {value: 60, dataPointText: '60'},
                  {value: 54, dataPointText: '54'},
                  {value: 85, dataPointText: '85'}
              ]}
                spacing={30}
                textColor1="yellow"
                textShiftY={-8}
                textShiftX={-10}
                textFontSize={13}
                thickness={5}
                hideRules
                pressEnabled
               
                
               // hideYAxisText
                yAxisColor="#0BA5A4"
                showVerticalLines
                verticalLinesColor="rgba(14,164,164,0.5)"
                xAxisColor="#0BA5A4"
                color="#0BA5A4"
            />
        </View>

            <View
        style={{
          backgroundColor: '#333340',
          paddingBottom: 40,
          borderRadius: 10,
          height:400,marginTop:50
        }}>
            <PieChart  data = {[
            {value: 54, color: '#177AD5'},
            {value: 40, color: '#79D2DE'},
            {value: 20, color: '#ED6665', shiftX: 28, shiftY: -18}
        ]} style={{marginTop:50}}/>
            </View>
            
       

            <View
        style={{
          backgroundColor: '#333340',
          paddingBottom: 40,
          borderRadius: 10,
          height:400,marginTop:50,paddingTop:60
        }}>


<BarChart data = {data} horizontal style={{marginTop:50}} />
        </View>


        <View
        style={{
          backgroundColor: '#333340',
          paddingBottom: 40,
          borderRadius: 10,
          height:400,marginTop:50
        }}>
<LineChart data = {data} areaChart />
</View >


<View
        style={{
          backgroundColor: '#333340',
          paddingBottom: 40,
          borderRadius: 10,
          height:400,marginTop:50
        }}>
<PieChart data = {data} donut style={{marginTop:50}} />
        </View>

</View>
            </ScrollView>
      
        </View>
    );

}


export default Dashboard;

