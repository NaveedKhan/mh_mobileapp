import React, { useState, useEffect } from 'react';
import { View, Text, Image, Dimensions, TouchableHighlight,TextInput, StatusBar, ScrollView,PermissionsAndroid,Platform } from 'react-native';
import { useSelector, useDispatch } from 'react-redux'
import { useNavigation } from '@react-navigation/native'
import { repoActions } from '../../actions';


function SplashScreen(){
    const navigation = useNavigation()
    const repoStates = useSelector(state => state.repo)
    let {repoList, loading, searchText} = repoStates;
    const dispatch = useDispatch()

    useEffect(() => {
        let that = this;
        setTimeout(function(){
           //navigation.navigate("Dashboard")
           //ChartsList
           //OverseasProgram
           navigation.navigate("ChartsList")
        },
         3000);
     }, [])

return(
    <View style={{ flex: 1, backgroundColor: '#e8e8e8' }}>
    <StatusBar hidden />
    <View style={{ height: Dimensions.get('window').height, justifyContent: 'center',
     alignItems: 'center', margin: 0,
      borderWidth:1 }} >
      <Image source={{uri:'https://mhstrategytracker.com/assets/images/logo.png'}} style={styles.Imagestyle} />
      <Text style={{ alignSelf: 'center', color: '#ef990e', fontSize: 17, fontWeight: 'bold' }}>
          Healthy Life at your finger Tip!
      </Text>

    </View>

    {/* <View style={{ flex: 3, justifyContent: 'center', alignItems: 'center' }}>
      {this.props.loading ? <Spinner /> : <View />}
      <Text numberOfLines={2} style={{ justifyContent: 'center', 
      alignItems: 'center', width: 250, textAlign: 'center', color: '#000', fontSize: 20 }}>
        One Stop Solution to all
                 your household needs...! </Text>
    </View> */}

  </View>

    );
}

export default SplashScreen;



const styles = {
    errorTextStyel: {
      fontSize: 17,
      alignSelf: 'center',
      color: 'red'
    },
    Imagestyle:
    {
      width: Dimensions.get('window').width / 1.3,
      height: Dimensions.get('window').height / 1.1,
      // borderRadius:( Dimensions.get('window').width/1.8)/2,
      resizeMode: 'contain'
      // opacity: 0.9
    }
  };