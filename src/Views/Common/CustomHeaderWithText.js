import React, {Component} from 'react';
import {View, Text, Dimensions,Image, TouchableHighlight} from 'react-native';
import {
  Icon,
} from 'native-base';

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

class CustomHeaderWithText extends Component {
  clickc() {
    debugger;
    Actions.pop();
  }

  backpressed() {
    debugger;

    Actions.pop();
  }

  render() {
    const {
      leftIcon,
      searchBar,
      rightIcon,
      refreshButton,
      refreshButtonClicked,
      headerStyle,
      headerTextStyle,
      iconStyle
    } = this.props;
    return (
<View style={{
    height: Dimensions.get('window').height / 8,
    //backgroundColor: '#4db79b', //'#f48004',
    //flexDirection: 'row',
    borderWidth:0,
    //zIndex:5
  }}>
         <Image
          source={require('../../Assets/Rectangle.png')}
          style={{
            height: Dimensions.get('window').height / 8,
            width: width * 1,
            resizeMode: 'cover',
          }}
        />

<View style={[styles.headerMainDiv,headerStyle]}>
        {leftIcon ? 
          <TouchableHighlight
          style={styles.leftIconDiv}
          onPress={() => this.backpressed()}>
          <Icon
            name="arrow-back"
            style={[{color: iconStyle ? '#000':'#fff', fontSize: 35, marginLeft: 10},iconStyle]}
          />
        </TouchableHighlight>
        :<View  style={styles.leftIconDiv}/>}
        <View style={styles.centerDiv}>
          <Text
            style={[{
              fontSize: 18,
              color: '#fff',
              alignSelf: 'center',
              fontWeight: 'bold',
              fontFamily:'Ariel'},headerTextStyle]
              }>
            {this.props.text}
          </Text>
        </View>
        {refreshButton ? (
          <TouchableHighlight
          style={styles.righIconDiv}
            onPress={refreshButtonClicked}>
            <View > 
              <Icon
              type="MaterialIcons"
                ios="ios-menu"
                android="md-menu"
                style={{color: '#f48004', fontSize: 35,alignSelf:'center'}}
              />
              <Text>Refresh</Text>
            </View>
          </TouchableHighlight>
        ) : (
          <View />
        )}
        {this.props.showMenu ? (
          <TouchableHighlight
            style={styles.righIconDiv}
            onPress={() => this.clickc()}>
            <Icon name="menu" style={{color: '#f48004', fontSize: 27}} />
          </TouchableHighlight>
        ) : (
          <View style={{flex: 1}} />
        )}
      </View>
      </View>
    );
  }
}

export {CustomHeaderWithText};

const styles = {
  headerMainDiv: {
    height: Dimensions.get('window').height / 8,
   // backgroundColor: '#4db79b', //'#f48004',
    flexDirection: 'row',
    borderWidth:0,
    position:'absolute'
    //zIndex:5
  },
  leftIconDiv: {
    backgroundColor: 'transparent',
    width: Dimensions.get('window').width /5,
    justifyContent: 'center',
    alignItems: 'stretch',
  },
  centerDiv: {
    backgroundColor: 'transparent',
    width: Dimensions.get('window').width /1.7,
    justifyContent: 'center',
    alignItems: 'center',
  },
  centerTextFieldDiv: {
    flexDirection: 'row',
    alignItems: 'center',
   // borderColor: 'gray',
    backgroundColor: '#fff',
    //borderWidth: 1,
    marginLeft: 10,
    marginRight: 10,
    borderRadius: 26,
  },
  righIconDiv: {
    width: Dimensions.get('window').width /5,
    backgroundColor: 'transparent',
    justifyContent: 'center',
    alignItems: 'center',
  },
};
