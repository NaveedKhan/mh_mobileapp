import {
    processColor,
  } from 'react-native';

const OverAllBenficieries = {
    values : [{
            value: 51,
            label: 'Female'
        },
        {
            value: 49,
            label: 'Male'
        },
    ],
    colors : [
        processColor('#ffc000'),
        processColor('#92d050'),
    ]
};

export {
    OverAllBenficieries
};