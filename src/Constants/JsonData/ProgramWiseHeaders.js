import {
    processColor,
  } from 'react-native';

 const ProgramWiseHeaders = {
     headersdata: [{
             Name: "Education",
             ImageUrl: "https://mhstrategytracker.com/assets/images/nicons/education.png",
             color: "#069960"
         },
         {
             Name: "Emergencies",
             ImageUrl: "https://mhstrategytracker.com/assets/images/nicons/emergency.png",
             color: "#f93750"
         },
         {
             Name: "Food Security",
             ImageUrl: "https://mhstrategytracker.com/assets/images/nicons/foodsecurity.png",
             color: "#ff9600"
         }, {
             Name: "Health",
             ImageUrl: "https://mhstrategytracker.com/assets/images/nicons/health.png",
             color: "#01939e"
         }, {
             Name: "Livelihoods",
             ImageUrl: "https://mhstrategytracker.com/assets/images/nicons/livelihood.png",
             color: "#d1a32a"
         }, {
             Name: "WASH",
             ImageUrl: "https://mhstrategytracker.com/assets/images/nicons/wash.png",
             color: "#00adff"
         }
     ]
 }

 export {
    ProgramWiseHeaders
 };