import {
    processColor,
  } from 'react-native';
const ProgramWiseBeneficieries = {
    values : [{
            value: 45,
            label: 'Education'
        },
        {
            value: 21,
            label: 'Emergencies'
        },
        {
            value: 15,
            label: 'Food Security'
        },
        {
            value: 9,
            label: 'Health'
        },
        {
            value: 15,
            label: 'Livelihoods'
        },
        {
            value: 15,
            label: 'WASH'
        }
    ],
    colors : [
        processColor('#069960'),
        processColor('#f93750'),
        processColor('#ff9600'),
        processColor('#01939e'),
        processColor('#d1a32a'),
        processColor('#00adff')
    ],
};

export {
    ProgramWiseBeneficieries
};